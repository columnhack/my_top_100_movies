# My Top One Hundred Movies

![Alt text here](./my_top_hundred_movies.svg)

# How to Run The Project

First, run the following command to compile the project

```
npm install
```

Then run

```
mvn start
```

# How to run the tests

Clone the repository and run the command
 ```
 npm run jest
 ```
this will first clean the project by running

The base url for accessing the live application is: http://18.170.0.36:7070/
The postman documentation is: https://www.postman.com/interstellar-space-182481/workspace/favorite-movies/collection/5870422-e9f25119-30d3-4979-9bd5-5a3aa8d94b0b?action=share&creator=5870422